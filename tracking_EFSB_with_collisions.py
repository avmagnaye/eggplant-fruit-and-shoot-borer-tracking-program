#
#   Author: Aaron Magnaye, Institute of Computer Science, UPLB
#   Description: A program that tracks Eggplant Fruit and Shoot borer and logs some of their activities in a text file.
#   How to run the program: python <programfile.py> <video_to_run.mp4/avi> <frame_skips(optional)>
#

import cv2          # version 3.4.2
import numpy as np  # version 1.15.1
import sys          # version AttributeError: module 'sys' has no attribute '__version__'

# 
# Image processing Helping Functions
# 

def split(image):  # FASTER SPLITTING ALGORITHM THAN cv2.split()
    '''
        Splits the images and returns the separate channels of the image
    '''
    return (image[:,:,0],image[:,:,1],image[:,:,2])

def scaleBoundingBox(x, y, width, height, scale):

    '''
        Scales the given bounding box according to the scale given
    '''

    x = x + width * (1 - scale)/2
    y = y + height * (1 - scale)/2
    width *= scale
    height *= scale
    return (int(x),int(y),int(width),int(height))

def drawObject(frame, obj, thickness=2):

    # draws the object on the frame
    iterations = len(obj["centroid"])-1

    # mark the centroid
    frame[int(obj["centroid"][iterations][1]), int(obj["centroid"][iterations][0])] = [0,255,0] 

    # draw the bounding box
    x,y,w,h = obj["search_area"][iterations]
    cv2.rectangle(frame, (x,y), (x+w,y+h), obj["color"], thickness)

    # draw the ellipse
    cv2.ellipse(frame, obj["bounding_ellipse"], obj["color"], thickness)

    # print(obj["bounding_ellipse"])

    # draw the bounding contour
    cv2.drawContours(frame, [obj["bounding_rect"]], -1, obj["color"], thickness)

def drawLines(frame):
    # Display Lines and Regions
    cv2.line(frame, (frame.shape[1]//3, 0), (frame.shape[1]//3, frame.shape[0]), (255, 0, 0), 1, 1)
    cv2.line(frame, (2*(frame.shape[1]//3), 0), (2*(frame.shape[1]//3), frame.shape[0]), (255, 0, 0), 1, 1)
    cv2.line(frame, (0, frame.shape[0]//3), (frame.shape[1], frame.shape[0]//3), (255, 0, 0), 1, 1)
    cv2.line(frame, (0, 2*(frame.shape[0]//3)), (frame.shape[1], 2*(frame.shape[0]//3)), (255, 0, 0), 1, 1)

    cv2.putText(frame, "Region 1", (frame.shape[1]//3 - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 2", (2*(frame.shape[1]//3) - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 3", (frame.shape[1] - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 4", (frame.shape[1]//3 - 70, 2*(frame.shape[0]//3) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 5", (frame.shape[1] - 70, 2*(frame.shape[0]//3) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 6", (frame.shape[1]//3 - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 7", (2*(frame.shape[1]//3) - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 8", (frame.shape[1] - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

def match_mask(obj1, obj2):

    '''
        Matches the two object masks and decides how much percentage is the lead_guitar getting accuracy from the whole band
    '''

    id1, mask1 = obj1
    id2, mask2 = obj2

    mask2 = cv2.resize(mask2, (mask1.shape[1],mask1.shape[0]))
    
    matched = cv2.bitwise_not(cv2.bitwise_xor(mask1, mask2))
    dim = matched.shape
    cv2.imshow("matched "+str(id1)+" and "+str(id2), matched)

    matched = [1 if x == 255 else 0 for x in matched.ravel()]

    percent_matched = np.sum(matched)
    percent_matched /= (dim[0]*dim[1])

    return (id1, id2, percent_matched)

def append_log(log_list, situation, second, obj, obj2 = None):

    # Log will contain the following information with the timestamp(number of frames into the video)
    # 1. when a worm enters a different region
    # 2. when a worm buries itself in an eggplant
    # 3. when a worm collides with another
    
    generated_log = ""

    obj_id = obj["id"]
    obj_color = obj["color"]
    obj_region = obj["region"][len(obj["region"]) - 1]

    if situation == 1:
        generated_log = "Worm #{} (colored: {}) has entered region no.{}.".format(obj_id, obj_color, obj_region)
    elif situation == 2:
        generated_log = "Worm #{} (colored: {}) has buried itself in an eggplant @ region no.{}.".format(obj_id, obj_color, obj_region)
    elif situation == 3:
        if obj2 == None:
            print("Error: No Obj2 passed")
            return 1
        else:
            obj2_id = obj2["id"]
            obj2_color = obj2["color"]
            generated_log = "Worm #{} (colored: {}) has collided with Worm #{} (colored: {}) @ region no.{}.".format(obj_id, obj_color, obj2_id, obj2_color, obj_region)

    print(generated_log)
    log_list.append((second,generated_log))

    return 0

#
#   Mathematical Helper functions
#

# given an update object from the program
def update_obj_stats(obj, toAppend, log, second):

    if toAppend == {} or obj["in_Eggplant"]:
        return

    if toAppend["area"] != None:
        factor_increased = (toAppend["area"] - obj["area"][len(obj["area"])  - 1]) / obj["area"][len(obj["area"])  - 1] 
        # print("factor increased: ", ( (toAppend["area"] - obj["area"][len(obj["area"])  - 1]) / obj["area"][len(obj["area"])  - 1] ) * 100)
        # print("toappend:",toAppend["area"])
        if factor_increased > 0.98:
            obj["in_Eggplant"] = True
            append_log(log, 2,second, obj)
        else:
            obj["area"].append(toAppend["area"])
    
    old_region = obj["region"][len(obj["region"]) - 1]
    obj["region"].append(toAppend["region"])

    # print("new obj region: ",toAppend
    # ["region"])    
    if toAppend["region"] != old_region:
        append_log(log, 1, second,obj)


    obj["centroid"].append(toAppend["centroid"])
    obj["search_area"].append(toAppend["search_area"])
    obj["bounding_rect"] = toAppend["bounding_rect"]
    obj["bounding_ellipse"] = toAppend["bounding_ellipse"]

def intersects(rect1, rect2):
    # rect = [[top_right,coordinate], [bottom_left_coordinate]]
    return not (rect1[0][0] < rect2[1][0] or rect1[1][0] > rect2[0][0] or rect1[0][1] < rect2[1][1] or rect1[1][1] > rect2[0][1])

def getDrawPoints(x, y, width, height):
    pt1 = (x,y)
    pt2 = (x+width, y+height)
    return (pt1, pt2)

def detectRegion(frame_width, frame_height, object_centroid):

    cX = object_centroid[0]
    cY = object_centroid[1]

    temp_coord = [1,1]

    temp_coord[0] = (0 if (cX < frame_width/3) else
                    1 if (cX >= frame_width/3) and (cX < (2*frame_width)/3) else
                    2)

    temp_coord[1] = (0 if (cY < frame_height/3) else
                    1 if (cY >= frame_height/3) and (cY < (2*frame_height)/3) else
                    2)

    return( 1 if temp_coord == [0,0] else
            2 if temp_coord == [1,0] else
            3 if temp_coord == [2,0] else
            4 if temp_coord == [0,1] else
            0 if temp_coord == [1,1] else
            5 if temp_coord == [2,1] else
            6 if temp_coord == [0,2] else
            7 if temp_coord == [1,2] else 
            8 ) 


video_name = sys.argv[1]
print("Opening file: ",video_name)

# Read video
video = cv2.VideoCapture(video_name)

# Exit if video not opened.
if not video.isOpened():
    print("Could not open video")
    sys.exit()

# Read first frame.
ok, og_first_frame = video.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

# turn first frame to grayscale


og_dim = (og_first_frame.shape[1], og_first_frame.shape[0])
dimensions = (1280,720)

resize = og_dim != dimensions

if resize: og_first_frame = cv2.resize(og_first_frame, dimensions)

bbox = cv2.selectROI(og_first_frame, False)

# get first_frame ROI
first_frame = og_first_frame[   int(bbox[1]):int(bbox[1]+bbox[3]),
                                int(bbox[0]):int(bbox[0]+bbox[2])]

# 
# Image Processing Techniques
# 

# convert to HLS
hls_ff = cv2.cvtColor(first_frame, cv2.COLOR_BGR2HLS)
h,l,saturation = split(hls_ff)
cv2.imshow("saturation", saturation)

# removes noise
gs_first_frame = cv2.bilateralFilter(saturation, 10, 25, 25)
cv2.imshow("bfilter", gs_first_frame)

ret, builtin1 = cv2.threshold(gs_first_frame, 90, 255, cv2.THRESH_BINARY_INV)
cv2.imshow("thres", builtin1)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
builtin1 = cv2.morphologyEx(builtin1, cv2.MORPH_CLOSE, kernel)
cv2.imshow("closing", builtin1)
builtin1 = cv2.morphologyEx(builtin1, cv2.MORPH_OPEN, kernel)
cv2.imshow("opening", builtin1)

# ret, labels, stats, centroids = cv2.connectedComponentsWithStats(builtin1, 8)
im2, contours, hierarchy = cv2.findContours(builtin1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# predefine the colors to be put on the worms // max of 4 worms
colors = [  (0,0,255),
            (255,255,0),
            (0,255,128),
            (255,0,128)]

objects_to_track = []

i = 0

ave_area = 0

# use the colored version of the image and color the first component green
for cnt in contours:
    
    if(i == 4):break

    color = colors[(i+1)%3]

    # convert the contour to world picture space
    cnt = np.array([[coord[0][0]+bbox[0], coord[0][1]+bbox[1]] for coord in cnt ])

    # get contour stats
    contour_moments = cv2.moments(cnt)

    ave_area += int(contour_moments['m00'])

    # create an object to be tracked
    obj = { "id" : i,   # object id
            "area" : [int(contour_moments['m00'])], # object area, will contain all the areas of the object per frame
            "centroid" : [( int(contour_moments['m10']/contour_moments['m00']), # object centroid list per frame
                            int(contour_moments['m01']/contour_moments['m00']))],
            "color" : color, # object assigned color // max of four objects so 4 colors
            "search_area" : [(scaleBoundingBox(*(cv2.boundingRect(cnt)), 1.5))], # search area is the scaled version of the bounding rectangle of the object
            "region" : [0], # origin region start, this is a list that will contain all the regions the object has been through
            "bounding_rect" : np.int0(cv2.boxPoints(cv2.minAreaRect(cnt))), # current bounding rectangle of the object
            "bounding_ellipse" : cv2.fitEllipse(cnt),
            "in_Eggplant" : False
    }
    
    objects_to_track.append(obj)
    
    i += 1

number_of_worms = len(objects_to_track)
if number_of_worms == 0:
    print("ERROR: no worms found")
    sys.exit() 
ave_area /= number_of_worms
print("worms: ", number_of_worms, "ave_area: ", ave_area)

ff = og_first_frame.copy()

for obj in objects_to_track:

    drawObject(ff, obj)


#
# FOR VIDEO PLAYER 
#

# fps = BUILTIN_SKIPS / SKIP # if skip == 0, then frames_counted = builtin_skips
# second = frame_no / fps

SKIP = 15               # frame skips
BUILTIN_SKIPS = 30      # 30 fps; native fps of the video

try:
    SKIP = int(sys.argv[2])
except Exception as e:
    print("frame skips: ", SKIP)

fps = int(BUILTIN_SKIPS / SKIP if SKIP > 0 else 1)

skipCount = 0           # current number of frame skips
collided_objects = []   # will contain the ids of the objects that are currently collided
reassign = False        # boolean that determines if there is a need to reassign values of objects to be tracked
to_reassign = []        # list that contains the objects to be reassigned
frame_count = 0         # number of frames
second = 0

# Log will contain the following information with the timestamp(number of frames into the video)
# 1. when a worm enters a different region
# 2. when a worm buries itself in an eggplant
# 3. when a worm collides with another

log = [] 

# print(saturation.shape)
temp = np.zeros((dimensions[1], dimensions[0]), dtype=np.uint8)           

while True:

    # Start Timer
    timer = cv2.getTickCount()

    # Read a new frame
    ok, frame = video.read()
    if not ok:
        break

    frame_count += 1

    if skipCount < SKIP:
        skipCount += 1
        continue
    elif skipCount >= SKIP:
        skipCount = 0

    second = frame_count / fps

    if resize: frame = cv2.resize(frame, dimensions)

    cv2.imshow("original frame", frame)

    # start tracker

    bounding_rectangles = []
    search_area_masks = []

    # cv2.waitKey(0)

    for obj in objects_to_track:

        if obj["in_Eggplant"]:
            continue


        # get the last search area from the object
        search_area = obj["search_area"][int(len(obj["search_area"]) - 1)]

        # slice the search area 
        cap_frame = frame[  int(search_area[1]):int(search_area[1]+search_area[3]),
                            int(search_area[0]):int(search_area[0]+search_area[2])]

        # the morphological operations will be done per capture_frame

        cv2.imshow("current cap_frame"+str(obj["id"]), cap_frame)

        # 
        # Image Processing Techniques
        # 

        # convert to HLS, continue processing using the saturation channel
        hls_ff = cv2.cvtColor(cap_frame, cv2.COLOR_BGR2HLS)
        h,l,saturation = split(hls_ff)

        # removes noise 
        filtered = cv2.bilateralFilter(saturation, 10, 25, 25)
        cv2.imshow("bfilter", gs_first_frame)

        # apply basic thresholding
        ret, builtin1 = cv2.threshold(filtered, 100, 255, cv2.THRESH_BINARY_INV)
        cv2.imshow("thres", builtin1)

        # close the spots then open to remove unecessary noise
        close_frame = cv2.morphologyEx(builtin1, cv2.MORPH_CLOSE, kernel)
        cv2.imshow("closing", builtin1)
        open_frame = cv2.morphologyEx(close_frame, cv2.MORPH_OPEN, kernel)

        # temp[0:open_frame.shape[0], 0:open_frame.shape[1]] = open_frame

        cv2.imshow("open_frame"+str(obj["id"]), open_frame)
        
        # add the current opened frame to the search area masks
        search_area_masks.append((obj["id"], open_frame))

        # get the components to be tracked using contours
        im2, contours, hierarchy = cv2.findContours(open_frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # compare the current obj to each component found in the search area
        # change object attributes based on returned object with the closest centroid distance to the current object
        
        prev_distance = frame.shape[1]          # max possible previous distance to the width of the image
        toAppend = {}                           # initialization of update_object

        # get only contours with areas greater than 25% of the average area of the initialized worms
        contours = [ cnt for cnt in contours if cv2.contourArea(cnt) > ave_area*0.25] 

        # If the contours of the image are more than 2, it means that there could be a collision since there are two objects in the search area
        # If there have already been confirmed objects that collided, it means if there are 2 contours detected, they are contours that are splitting
        if len(contours) > 1 and len(collided_objects) > 0:    
            # print("moving away")
            reassign = True         # boolean that enables the reassignation of objects that just split up

        # at the end of this loop, an object with all the attributes for the next frame will be created
        for cnt in contours:    

            # convert the contour to world picture space
            new_cnt = np.array([[coord[0][0]+search_area[0], coord[0][1]+search_area[1]] for coord in cnt ])

            # get contour stats
            contour_moments = cv2.moments(new_cnt)

            # get the centroid to be compared to an object's centroid
            centroid_to_check = (int(contour_moments['m10']/contour_moments['m00']), int(contour_moments['m01']/contour_moments['m00']))

            # get the distance of the current centroid from the centroid of the current object
            a = np.array(obj["centroid"][len(obj["centroid"]) - 1])
            b = np.array(centroid_to_check)
            dist = np.linalg.norm(a-b)

            # If reassign is true, it means that all the objects that were detected as contours are objects that actually came from a collision and now 
            # they just split up
            if reassign:

                # a new object with stats to be used to update the current object is made, toAppend will be replaced if a better object is determined
                toAppend = {   
                    "area" : contour_moments["m00"],
                    "centroid" :  centroid_to_check,
                    "search_area" : (scaleBoundingBox(*(cv2.boundingRect(new_cnt)), 1.5)),
                    "region" : detectRegion(frame.shape[1], frame.shape[0], centroid_to_check),
                    "bounding_rect" : np.int0(cv2.boxPoints(cv2.minAreaRect(new_cnt))),
                    "bounding_ellipse" : cv2.fitEllipse(new_cnt)
                }

                # list of all update objects for reassigning of object to be tracked
                to_reassign.append(toAppend)

            else:

                # the contour with the closest centroid to the current object's centroid will be chosen
                if dist < prev_distance: # If a new possible object is found, create a new object pertaining to it

                    # a new object with stats to be used to update the current object is made, toAppend will be replaced if a better object is determined
                    toAppend = {   
                        "area" : contour_moments["m00"] if obj["id"] not in collided_objects else None,
                        "centroid" :  centroid_to_check,
                        "search_area" : (scaleBoundingBox(*(cv2.boundingRect(new_cnt)), 1.5)),
                        "region" : detectRegion(frame.shape[1], frame.shape[0], centroid_to_check),
                        "bounding_rect" : np.int0(cv2.boxPoints(cv2.minAreaRect(new_cnt))),
                        "bounding_ellipse" : cv2.fitEllipse(new_cnt)
                    }

                    prev_distance = dist

        # reassigns the objects to be reassigned to tracking objects after they split up, 
        if reassign:

            # for all objects that have the same id in the collided_objects list, they are candidates to receive updates
            # from the to_reassign stats_update list
            for obj in objects_to_track:
                for idname in collided_objects:
                    if obj["id"] == idname:

                        # get the average area of the object
                        try:
                            ave_area_of_obj = np.sum(obj["area"]) / len(obj["area"])
                        except Exception as e:
                            print("area of obj has None \n" if None in obj["area"] else "", end="")
                            raise e
                        
                        # find the object that has the closest area to reassign back to the original object
                        prev_area_diff = 999999
                        for obj_to_assign in to_reassign:
                            curr_area_diff = abs(obj_to_assign["area"] - ave_area_of_obj)
                            if prev_area_diff > curr_area_diff:
                                toAppend = obj_to_assign
                                prev_area_diff = curr_area_diff

                        # append the stats of the selected object
                        update_obj_stats(obj, toAppend, log, second)
                        drawObject(frame, obj, thickness=1)

                        # remove the selected object from the list
                        to_reassign.remove(toAppend)
            
            # resets the temp variables
            collided_objects = []
            reassign = False

        # else, updates the current tracked object's position and drawing  
        else:
            # append the stats of the new object to the current object
            update_obj_stats(obj, toAppend, log, second)
            drawObject(frame, obj, thickness=1)

        
    # checks for collision using masking
    
    passed = [] # masks that have not been matched yet

    # This loop iterates over all possible search_area_mask combinations
    # and checks if there are matches in search areas
    #   if search_areas match between two objects, that must mean that they are looking 
    #   at the same object and thus the two objects they were tracking has already collided
    for m1, mask in enumerate(search_area_masks):
        for m2, mask2 in enumerate(search_area_masks):

            # this filter does not let mask combinations to be paired again 
            # after they have already been compared
            combination = [m1, m2]
            combination.sort()
            if combination in passed or m1 == m2:
                continue
            else:
                passed.append(combination)

            # matches the two masks and gets the percentage of likeness
            id1, id2, matched = match_mask(mask, mask2)

            # if the likeness is more than 98%, that must mean that 
            # the two masks are looking at the same thing
            if matched > 0.98:

                print(id1," and ",id2," collided")

                # add the ids of the collided objects to the collided_objects list
                if id1 not in collided_objects:
                    collided_objects.append(id1)
                if id2 not in collided_objects:
                    collided_objects.append(id2)
            
    # end tracker

    # Calculate Frames per second (FPS)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

    # Display FPS on frame
    cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

    # Draw the region lines
    drawLines(frame)

    # Display result
    cv2.imshow("frame", frame)

    # debug
    # cv2.waitKey(0)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27 : break

# print(len(log))

f = open("log_latest_run.txt","w")
string_to_write = ""
for line in log:
    string_to_write = string_to_write + "Second #{} | {} \n".format(line[0], line[1])
f.write(string_to_write)
# print(string_to_write)
f.close()

# cv2.waitKey(0)
# cv2.destroyAllWindows()