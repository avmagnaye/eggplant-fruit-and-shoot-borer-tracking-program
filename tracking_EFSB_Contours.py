import cv2
import numpy as np
import sys

video_name = sys.argv[1]
print("Opening file: ",video_name)

def split(image):  # FASTER SPLITTING ALGORITHM THAN cv2.split()
    return (image[:,:,0],image[:,:,1],image[:,:,2])

def update_obj_stats(obj, toAppend):

    if toAppend == {}:
        return

    obj["area"] = toAppend["area"]
    obj["centroid"].append(toAppend["centroid"])
    obj["search_area"].append(toAppend["search_area"])
    obj["region"] = toAppend["region"]
    obj["bounding_rect"] = toAppend["bounding_rect"]
    obj["bounding_ellipse"] = toAppend["bounding_ellipse"]

def thresholding(update):
    # print(update)
    threshold = cv2.getTrackbarPos('threshold',"Thresholding")

    ret, builtin1 = cv2.threshold(gs_first_frame, threshold, 255, cv2.THRESH_BINARY_INV)

    cv2.imshow("Thresholding", builtin1)

def intersects(rect1, rect2):
    # rect = [[top_right,coordinate], [bottom_left_coordinate]]
    return not (rect1[0][0] < rect2[1][0] or rect1[1][0] > rect2[0][0] or rect1[0][1] < rect2[1][1] or rect1[1][1] > rect2[0][1])

def scaleBoundingBox(x, y, width, height, scale):
    x = x + width * (1 - scale)/2
    y = y + height * (1 - scale)/2
    width *= scale
    height *= scale
    # print(int(x),int(y),int(width),int(height))
    return (int(x),int(y),int(width),int(height))

def getDrawPoints(x, y, width, height):
    pt1 = (x,y)
    pt2 = (x+width, y+height)
    return (pt1, pt2)

def detectRegion(frame_width, frame_height, object_centroid):

    cX = object_centroid[0]
    cY = object_centroid[1]

    temp_coord = [1,1]

    region_assigned = 0

    if cX < frame_width/3:
        temp_coord[0] = 0
    elif cX > frame_width/3 and cX < (2*frame_width)/2:
        temp_coord[0] = 1
    else:
        temp_coord[0] = 2

    if cY < frame_height/3:
        temp_coord[1] = 0
    elif cY > frame_height/3 and cY < (2*frame_height)/2:
        temp_coord[1] = 1
    else:
        temp_coord[1] = 2

    if temp_coord == [0,0]:
        return 1
    elif temp_coord == [1,0]:
        return 2
    elif temp_coord == [2,0]:
        return 3
    elif temp_coord == [0,1]:
        return 4
    elif temp_coord == [1,1]:
        return 0
    elif temp_coord == [2,1]:
        return 5
    elif temp_coord == [0,2]:
        return 6
    elif temp_coord == [1,2]:
        return 7
    elif temp_coord == [2,2]:
        return 8

def drawObject(frame, obj, thickness=2):

    # draws the object on the frame
    iterations = len(obj["centroid"])-1

    # mark the centroid
    frame[int(obj["centroid"][iterations][1]), int(obj["centroid"][iterations][0])] = [0,255,0] 

    # draw the bounding box
    x,y,w,h = obj["search_area"][iterations]
    cv2.rectangle(frame, (x,y), (x+w,y+h), obj["color"], thickness)

    # draw the ellipse
    cv2.ellipse(frame, obj["bounding_ellipse"], obj["color"], thickness)

    # print(obj["bounding_ellipse"])

    # draw the bounding contour
    cv2.drawContours(frame, [obj["bounding_rect"]], -1, obj["color"], thickness)


# Read video
video = cv2.VideoCapture(video_name)

# Exit if video not opened.
if not video.isOpened():
    print("Could not open video")
    sys.exit()

# Read first frame.
ok, og_first_frame = video.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

# turn first frame to grayscale

bbox = cv2.selectROI(og_first_frame, False)

# get first_frame ROI
first_frame = og_first_frame[   int(bbox[1]):int(bbox[1]+bbox[3]),
                                int(bbox[0]):int(bbox[0]+bbox[2])]

# 
# Image Processing Techniques
# 

# convert to HLS
hls_ff = cv2.cvtColor(first_frame, cv2.COLOR_BGR2HLS)
h,l,saturation = split(hls_ff)
# cv2.imshow("saturation", saturation)

# removes noise
gs_first_frame = cv2.bilateralFilter(saturation, 10, 25, 25)
# cv2.imshow("bfilter", gs_first_frame)

ret, builtin1 = cv2.threshold(gs_first_frame, 110, 255, cv2.THRESH_BINARY_INV)
# cv2.imshow("thres", builtin1)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
builtin1 = cv2.morphologyEx(builtin1, cv2.MORPH_CLOSE, kernel)
# cv2.imshow("closing", builtin1)
builtin1 = cv2.morphologyEx(builtin1, cv2.MORPH_OPEN, kernel)
cv2.imshow("opening", builtin1)

# ret, labels, stats, centroids = cv2.connectedComponentsWithStats(builtin1, 8)
im2, contours, hierarchy = cv2.findContours(builtin1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# predefine the colors to be put on the worms // max of 4 worms
colors = [  (0,0,255),
            (255,255,0),
            (0,255,128),
            (255,0,128)]

objects_to_track = []

i = 0

ave_area = 0

# use the colored version of the image and color the first component green
for cnt in contours:
    
    if(i == 4):break

    color = colors[(i+1)%3]

    # convert the contour to world picture space
    cnt = np.array([[coord[0][0]+bbox[0], coord[0][1]+bbox[1]] for coord in cnt ])

    # get contour stats
    contour_moments = cv2.moments(cnt)

    ave_area += int(contour_moments['m00'])

    # create an object to be tracked
    obj = { "id" : i,   # object id
            "area" : int(contour_moments['m00']), # object area
            "centroid" : [( int(contour_moments['m10']/contour_moments['m00']), # object centroid
                            int(contour_moments['m01']/contour_moments['m00']))],
            "color" : color, # object assigned color // max of four objects so 4 colors
            "search_area" : [(scaleBoundingBox(*(cv2.boundingRect(cnt)), 1.5))], # search area is the scaled version of the bounding rectangle of the object
            "region" : 0, # origin region
            "bounding_rect" : np.int0(cv2.boxPoints(cv2.minAreaRect(cnt))), # current bounding rectangle of the object
            "bounding_ellipse" : cv2.fitEllipse(cnt)
    }
    
    objects_to_track.append(obj)
    
    i += 1

ff = og_first_frame.copy()

for obj in objects_to_track:

    drawObject(ff, obj)

number_of_worms = len(objects_to_track) 
ave_area /= number_of_worms
print("worms: ", number_of_worms, "ave_area: ", ave_area)

# cv2.imshow("ff",ff)

#
# FOR VIDEO PLAYER 
#

skipCount = 0
skip = 15
frame_number = 0

while True:

    # Start Timer
    timer = cv2.getTickCount()

    # Read a new frame
    ok, frame = video.read()
    if not ok:
        break

    if skipCount < skip:
        skipCount += 1
        continue
    elif skipCount >= skip:
        skipCount = 0

    cv2.imshow("original frame", frame)

    # start tracker

    colored_worms = np.zeros(frame.shape, np.uint8)
    bounding_rectangles = []

    for obj in objects_to_track:

        # get the last search area from the object
        search_area = obj["search_area"][int(len(obj["search_area"]) - 1)]

        # slice the search area 
        cap_frame = frame[  int(search_area[1]):int(search_area[1]+search_area[3]),
                            int(search_area[0]):int(search_area[0]+search_area[2])]

        # the morphological operations will be done per capture_frame

        cap_col_worms = colored_worms[  int(search_area[1]):int(search_area[1]+search_area[3]),
                                        int(search_area[0]):int(search_area[0]+search_area[2])]
                            
        cv2.imshow("current cap_frame"+str(obj["id"]), cap_frame)

        # 
        # Image Processing Techniques
        # 

        # convert to HLS
        hls_ff = cv2.cvtColor(cap_frame, cv2.COLOR_BGR2HLS)
        h,l,saturation = split(hls_ff)
        # cv2.imshow("saturation", saturation)

        # removes noise
        filtered = cv2.bilateralFilter(saturation, 10, 25, 25)
        # cv2.imshow("bfilter", gs_first_frame)

        ret, builtin1 = cv2.threshold(filtered, 100, 255, cv2.THRESH_BINARY_INV)
        # cv2.imshow("thres", builtin1)

        # close the spots then open to remove unecessary noise
        close_frame = cv2.morphologyEx(builtin1, cv2.MORPH_CLOSE, kernel)
        # cv2.imshow("closing", builtin1)
        open_frame = cv2.morphologyEx(close_frame, cv2.MORPH_OPEN, kernel)
        # cv2.imshow("opening", builtin1)

        # get the components tracked using contours
        im2, contours, hierarchy = cv2.findContours(open_frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # cv2.drawContours(open_frame, contours, -1, obj["color"], 2)

        cv2.imshow("open_frame"+str(obj["id"]), open_frame)

        # compare the current obj to each component found in the search area
        # change object attributes based on returned object with the closest centroid distance to the current object
        
        prev_distance = frame.shape[1] # max possible previous distance to the width of the image
        toAppend = {}                   # initialization of update_object

        # contours = [cnt for cnt in contours if cv2.contourArea(cnt) > 0.4]

        for cnt in contours:    # at the end of this loop, an object with all the attributes for the next frame will be created

            if(cv2.contourArea(cnt) > ave_area*0.4):   # get only contours with areas greater than 40% of the average area of the initialized worms
            
                # convert the contour to world picture space
                cnt = np.array([[coord[0][0]+search_area[0], coord[0][1]+search_area[1]] for coord in cnt ])

                # get contour stats
                contour_moments = cv2.moments(cnt)

                # get the centroid to be compared to an object's centroid
                centroid_to_check = (int(contour_moments['m10']/contour_moments['m00']), int(contour_moments['m01']/contour_moments['m00']))

                # get the distance of the current centroid from the centroid of the current object
                a = np.array(obj["centroid"][len(obj["centroid"]) - 1])
                b = np.array(centroid_to_check)
                dist = np.linalg.norm(a-b)

                # the contour with the closest centroid to the current object's centroid will be chosen
                
                if dist < prev_distance: # If a new possible object is found, create a new object pertaining to it

                    toAppend = {   # a new object with stats to be used to update the current object is made, toAppend will be replaced if a better object is determined
                        "area" : contour_moments["m00"],
                        "centroid" :  centroid_to_check,
                        "search_area" : (scaleBoundingBox(*(cv2.boundingRect(cnt)), 1.5)),
                        "region" : detectRegion(frame.shape[1], frame.shape[0], centroid_to_check),
                        "bounding_rect" : np.int0(cv2.boxPoints(cv2.minAreaRect(cnt))),
                        "bounding_ellipse" : cv2.fitEllipse(cnt)
                    }

                    prev_distance = dist

        # append the stats of the new object to the current object
        update_obj_stats(obj, toAppend)
        drawObject(frame, obj, thickness=1)
        drawObject(colored_worms, obj, thickness=1)


    # end tracker

    # Calculate Frames per second (FPS)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

    # Display FPS on frame
    cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

    # Display Lines and Regions
    cv2.line(frame, (frame.shape[1]//3, 0), (frame.shape[1]//3, frame.shape[0]), (255, 0, 0), 1, 1)
    cv2.line(frame, (2*(frame.shape[1]//3), 0), (2*(frame.shape[1]//3), frame.shape[0]), (255, 0, 0), 1, 1)
    cv2.line(frame, (0, frame.shape[0]//3), (frame.shape[1], frame.shape[0]//3), (255, 0, 0), 1, 1)
    cv2.line(frame, (0, 2*(frame.shape[0]//3)), (frame.shape[1], 2*(frame.shape[0]//3)), (255, 0, 0), 1, 1)

    cv2.putText(frame, "Region 1", (frame.shape[1]//3 - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 2", (2*(frame.shape[1]//3) - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 3", (frame.shape[1] - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 4", (frame.shape[1]//3 - 70, 2*(frame.shape[0]//3) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 5", (frame.shape[1] - 70, 2*(frame.shape[0]//3) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 6", (frame.shape[1]//3 - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 7", (2*(frame.shape[1]//3) - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 8", (frame.shape[1] - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

    # Display result
    cv2.imshow("frame", frame)
    cv2.imshow("colored", colored_worms)

    # debug
    cv2.waitKey(0)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27 : break

# cv2.waitKey(0)
# cv2.destroyAllWindows()