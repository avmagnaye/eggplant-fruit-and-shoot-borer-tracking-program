import cv2
import numpy as np
import sys

# Read video
video = cv2.VideoCapture("tracking_efsb.mp4")

# Exit if video not opened.
if not video.isOpened():
    print("Could not open video")
    sys.exit()

# Read first frame.
ok, first_frame = video.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

# turn first frame to grayscale
first_frame = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)

bbox = cv2.selectROI(first_frame, False)

# print(bbox)

# detect features
sift = cv2.xfeatures2d.SIFT_create()

    # get first_frame ROI
first_frame = first_frame[  int(bbox[1]):int(bbox[1]+bbox[3]),
                            int(bbox[0]):int(bbox[0]+bbox[2])]


kp_image, desc_image = sift.detectAndCompute(first_frame, None)
first_frame = cv2.drawKeypoints(first_frame, kp_image, first_frame)

# cv2.imshow("detect features", first_frame)

# feature matching
index_params = dict(algorithm=0, trees=5)
search_params = dict()
flann = cv2.FlannBasedMatcher(index_params, search_params)

while True:

    # Read a new frame
    ok, frame = video.read()
    if not ok:
        break

    # Start Timer
    timer = cv2.getTickCount()


    # cv2.imshow("Tracking", frame)

    # Tracking
    grayframe = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    kp_grayframe, desc_grayframe = sift.detectAndCompute(grayframe, None)
    matches = flann.knnMatch(desc_image, desc_grayframe, k=2)    

    # frame = cv2.drawKeypoints(frame, kp_grayframe, grayframe) 
    # grayframe = cv2.drawKeypoints(grayframe, kp_grayframe, grayframe) 

    good_points = []
    for m, n in matches:
        if m.distance < 0.6*n.distance:
            good_points.append(m)

    # Calculate Frames per second (FPS)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);
    # Display FPS on frame
    cv2.putText(grayframe, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2);

    matched_features = cv2.drawMatches(first_frame, kp_image, grayframe, kp_grayframe, good_points, grayframe)

    # print("first_Frame shape: ", first_frame.shape)

    # homography
    # if len(good_points) > 3:
    #     query_pts = np.float32([kp_image[m.queryIdx].pt for m in good_points]).reshape(-1, 1, 2)
    #     train_pts = np.float32([kp_grayframe[m.trainIdx].pt for m in good_points]).reshape(-1, 1, 2)

    #     matrix, mask = cv2.findHomography(query_pts, train_pts, cv2.RANSAC, 5.0)
    #     matches_mask = mask.ravel().tolist()

    #     # perspective transform
    #     h, w, s= first_frame.shape
    #     pts = np.float32([[0, 0], [0, h], [w, h], [w, 0]]).reshape(-1, 1, 2)
    #     dst = cv2.perspectiveTransform(pts, matrix)
    #     homography = cv2.polylines(grayframe, [np.int32(dst)], True, (255, 0, 0), 3)
    #     cv2.imshow("Homography", homography)
    # else:
    #     cv2.imshow("Homography", grayframe)


    # Display result
    cv2.imshow("matched_features", matched_features)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27 : break