import cv2
import numpy as np
import sys

video_name = sys.argv[1]
print("Opening file: ",video_name)


def thresholding(update):
    # print(update)
    threshold = cv2.getTrackbarPos('threshold',"Thresholding")

    ret, builtin1 = cv2.threshold(gs_first_frame, threshold, 255, cv2.THRESH_BINARY_INV)

    cv2.imshow("Thresholding", builtin1)

def scaleBoundingBox(x, y, width, height, scale):
    x = x + width * (1 - scale)/2
    y = y + height * (1 - scale)/2
    width *= scale
    height *= scale
    return (int(x),int(y),int(width),int(height))

def getDrawPoints(x, y, width, height):
    pt1 = (x,y)
    pt2 = (x+width, y+height)
    return (pt1, pt2)

def detectRegion(frame_width, frame_height, object_centroid):

    cX = object_centroid[0]
    cY = object_centroid[1]

    temp_coord = [1,1]

    region_assigned = 0

    if cX < frame_width/3:
        temp_coord[0] = 0
    elif cX > frame_width/3 and cX < (2*frame_width)/2:
        temp_coord[0] = 1
    else:
        temp_coord[0] = 2

    if cY < frame_height/3:
        temp_coord[1] = 0
    elif cY > frame_height/3 and cY < (2*frame_height)/2:
        temp_coord[1] = 1
    else:
        temp_coord[1] = 2

    if temp_coord == [0,0]:
        return 1
    elif temp_coord == [1,0]:
        return 2
    elif temp_coord == [2,0]:
        return 3
    elif temp_coord == [0,1]:
        return 4
    elif temp_coord == [1,1]:
        return 0
    elif temp_coord == [2,1]:
        return 5
    elif temp_coord == [0,2]:
        return 6
    elif temp_coord == [1,2]:
        return 7
    elif temp_coord == [2,2]:
        return 8

def drawObject(frame, obj):

    # draws the object on the frame

    # print("obj [",obj["id"],"] area : ",obj["area"])

    centroid_len = len(obj["centroid"])

    frame[int(obj["centroid"][centroid_len - 1][1]), int(obj["centroid"][centroid_len - 1][0])] = [0,255,0] # mark the centroid

    stats = obj["searchArea"]
    pt1 = (stats[0],stats[1])
    pt2 = (stats[0]+stats[2], stats[1]+stats[3])

    cv2.rectangle(frame, pt1, pt2, obj["color"], thickness=1, lineType=8) # draw the searchArea



# Read video
video = cv2.VideoCapture(video_name)

# Exit if video not opened.
if not video.isOpened():
    print("Could not open video")
    sys.exit()

# Read first frame.
ok, og_first_frame = video.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

# turn first frame to grayscale

bbox = cv2.selectROI(og_first_frame, False)

# get first_frame ROI
first_frame = og_first_frame[   int(bbox[1]):int(bbox[1]+bbox[3]),
                                int(bbox[0]):int(bbox[0]+bbox[2])]

# convert to Gray
gs_first_frame = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)

# removes noise
gs_first_frame = cv2.medianBlur(gs_first_frame, 5)

ret, builtin1 = cv2.threshold(gs_first_frame, 125, 255, cv2.THRESH_BINARY_INV)

kernel = np.ones((5,5),np.uint8)
builtin1 = cv2.morphologyEx(builtin1, cv2.MORPH_OPEN, kernel)

cv2.imshow("Thresholding", builtin1)

ret, labels, stats, centroids = cv2.connectedComponentsWithStats(builtin1, 8)

# code to get each connected component
    
colors = [  (0,0,255),
            (255,255,0),
            (0,255,128),
            (255,0,128)]

objects_to_track = []

# use the colored version of the image and color the first component green
for i in range(1, ret):
    
    if(i == 4):break

    color = colors[(i+1)%3]

    obj = { "id" : i,
            "area" : stats[i,4],
            "centroid" : [(centroids[i][0],centroids[i][1])],
            "color" : color,
            "searchArea" : [stats[i,0] + bbox[0], # relative to the bigger picture
                            stats[i,1] + bbox[1],
                            stats[i,2],
                            stats[i,3]],
            "region" : 0,
            "searchArea_hist": []
    }
    
    objects_to_track.append(obj)

ff = og_first_frame.copy()

for obj in objects_to_track:

    drawObject(ff, obj)

#
#   FOR VIDEO PLAYER 
#

skipCount = 0
skip = 15

while True:

    # Start Timer
    timer = cv2.getTickCount()

    # Read a new frame
    ok, frame = video.read()
    if not ok:
        break

    if skipCount < skip:
        skipCount += 1
        continue
    elif skipCount >= skip:
        skipCount = 0

    # start tracker

    colored_worms = np.zeros(frame.shape, np.uint8)
    bounding_rectangles = []

    for obj in objects_to_track:

        search_area = scaleBoundingBox(obj["searchArea"][0],
                                        obj["searchArea"][1],
                                        obj["searchArea"][2],
                                        obj["searchArea"][3],
                                        1.5
                                        )
        color_to_draw = obj["color"]
        

        cap_frame = frame[  int(search_area[1]):int(search_area[1]+search_area[3]),
                            int(search_area[0]):int(search_area[0]+search_area[2])] # get the new search area
        

        cap_col_worms = colored_worms[  int(search_area[1]):int(search_area[1]+search_area[3]),
                            int(search_area[0]):int(search_area[0]+search_area[2])]

        # convert the capture frame to gray
        gray_frame = cv2.cvtColor(cap_frame, cv2.COLOR_BGR2GRAY)
    
        # remove noise
        blur_frame = cv2.medianBlur(gray_frame, 5)

        # apply thresholding
        ret, thresh_frame = cv2.threshold(blur_frame, 125, 255, cv2.THRESH_BINARY_INV)

        # open the picture to remove further outside noise
        kernel = np.ones((5,5),np.uint8)
        open_frame = cv2.morphologyEx(thresh_frame, cv2.MORPH_OPEN, kernel)

        cv2.imshow("open_frame"+str(obj["id"]), open_frame)

        # get the components tracked
        ret, labels, stats, centroids = cv2.connectedComponentsWithStats(open_frame, 8)

        # compare the current obj to each component found in the search area
        # change object attributes based on returned object with the closest centroid distance to the current object
        prev_distance = frame.shape[1]

        for i in range(1, ret):

            if(stats[i,4] > 100):   # get only labels with areas greater than 100 pixels

                centroid_to_check = (centroids[i][0],centroids[i][1])

                a = np.array(obj["centroid"][len(obj["centroid"]) - 1])
                b = np.array(centroid_to_check)

                dist = np.linalg.norm(a-b)

                if dist < prev_distance:

                    obj["area"] = stats[i, 4]
                    obj["centroid"].append((centroid_to_check[0] + search_area[0], centroid_to_check[1] + search_area[1]))
                    obj["searchArea"] = [   stats[i,0] + search_area[0], 
                                            stats[i,1] + search_area[1],
                                            stats[i,2],
                                            stats[i,3]]
                    obj["searchArea_hist"].append(obj["searchArea"])

                    idd = obj["id"]

                    if idd == 2:
                        print("objects found:", ret)

                    region = detectRegion(frame.shape[1], frame.shape[0], obj["centroid"][len(obj["centroid"]) - 1])

                    if region != obj["region"]:
                        print("object [",idd,"] entered region: ", region)
                        obj["region"] = region

                    cap_col_worms[labels==i] = obj["color"]

        cv2.rectangle(frame, *getDrawPoints(*search_area), color_to_draw, thickness=1, lineType=8)
        drawObject(frame, obj)

        colored_worms[int(search_area[1]):int(search_area[1]+search_area[3]),
                int(search_area[0]):int(search_area[0]+search_area[2])] = cap_col_worms


    # end tracker

    # Calculate Frames per second (FPS)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

    # Display FPS on frame
    cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

    # Display Lines and Regions
    cv2.line(frame, (frame.shape[1]//3, 0), (frame.shape[1]//3, frame.shape[0]), (255, 0, 0), 1, 1)
    cv2.line(frame, (2*(frame.shape[1]//3), 0), (2*(frame.shape[1]//3), frame.shape[0]), (255, 0, 0), 1, 1)
    cv2.line(frame, (0, frame.shape[0]//3), (frame.shape[1], frame.shape[0]//3), (255, 0, 0), 1, 1)
    cv2.line(frame, (0, 2*(frame.shape[0]//3)), (frame.shape[1], 2*(frame.shape[0]//3)), (255, 0, 0), 1, 1)

    cv2.putText(frame, "Region 1", (frame.shape[1]//3 - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 2", (2*(frame.shape[1]//3) - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 3", (frame.shape[1] - 70, frame.shape[0]//3 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 4", (frame.shape[1]//3 - 70, 2*(frame.shape[0]//3) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 5", (frame.shape[1] - 70, 2*(frame.shape[0]//3) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 6", (frame.shape[1]//3 - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 7", (2*(frame.shape[1]//3) - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.putText(frame, "Region 8", (frame.shape[1] - 70, frame.shape[0] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

    # Display result
    cv2.imshow("frame", frame)
    cv2.imshow("colored", colored_worms)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27 : break

# cv2.waitKey(0)
# cv2.destroyAllWindows()